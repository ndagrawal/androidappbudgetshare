package com.nilesh_ameya.fshare;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by nileshagrawal on 6/18/13.
 */
public class DatabaseImplementation {

    //This class will do the database implementation of the first view . i.e. of the Create New Shopping Assingment.

    public static final String KEY_ROWID ="id";
    public static final String KEY_NAME ="ColumnNme";
    public static final String KEY_DATE ="Date";


    private static final String DATABASE_NAME="Shopping";
    private static final String DATABASE_TABLE="ShopName";
    private static final int DATABASE_VERSION =1;

    public DBHelper ourHelper;
    public final Context ourContext;
    public SQLiteDatabase ourDatabase;



    private class DBHelper extends SQLiteOpenHelper {

        //Make a constructor of the class....

        public DBHelper(Context context){

            super(context,DATABASE_NAME,null,DATABASE_VERSION);
            //Add the unoimplemented methods ..

    }


        @Override
        public void onCreate(SQLiteDatabase db) {
            //Will Create a table when this is called for created..
            db.execSQL("CREATE TABLE "+ DATABASE_TABLE +" ("+
                        KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                    KEY_NAME + " TEXT NOT NULL, "+KEY_DATE +" TEXT NOT NULL");

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int i, int i2) {
         db.execSQL("DROP TABLE IF EXISTS "+ DATABASE_TABLE);
         onCreate(db);

        }

    }

    public DatabaseImplementation(Context c) {
        ourContext = c;
    }

    public DatabaseImplementation open()
    {
        ourHelper = new DBHelper(ourContext);
        ourDatabase= ourHelper.getWritableDatabase();

        return this;
    }

    public void close()
    {
        ourHelper.close();

    }


}
